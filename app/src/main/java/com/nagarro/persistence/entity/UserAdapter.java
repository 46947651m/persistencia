package com.nagarro.persistence.entity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nagarro.persistence.R;

import java.util.List;

/*public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private List<User> items;
    private Context context;

    public  class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView firstName;
        public TextView lastName;
        public TextView age;

        public UserViewHolder(View v) {
            super(v);
            firstName = (TextView) v.findViewById(R.id.textViewFirstName);
            lastName = (TextView) v.findViewById(R.id.textViewLastName);
            age = (TextView) v.findViewById(R.id.textViewAge);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public UserAdapter(List<User> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.usuari, viewGroup, false);
        return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(UserViewHolder viewHolder, int i) {
        viewHolder.firstName.setText(items.get(i).getFirstName());
        viewHolder.lastName.setText(items.get(i).getLastName());
        viewHolder.age.setText(String.valueOf(items.get(i).getAge()));
    }
}*/
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    class UserViewHolder extends RecyclerView.ViewHolder {
        private final TextView userItemView;

        private UserViewHolder(View itemView) {
            super(itemView);
            userItemView = itemView.findViewById(R.id.textViewFirstName);
        }
    }

    private final LayoutInflater mInflater;
    private List<User> mUsers; // Cached copy of words

    public UserAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.usuari, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        if (mUsers != null) {
            User current = mUsers.get(position);
            holder.userItemView.setText(current.getFirstName());
        } else {
            // Covers the case of data not being ready yet.
            holder.userItemView.setText("No data");
        }
    }

    public void setUsers(List<User> users){
        mUsers = users;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mUsers != null)
            return mUsers.size();
        else return 0;
    }
}