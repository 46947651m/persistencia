package com.nagarro.persistence.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import android.support.annotation.NonNull;

import com.nagarro.persistence.entity.User;

import java.util.List;

public class UserViewModel extends AndroidViewModel {
    private UserRepository mRepository;
    private List<User> mAllUsers;

    public UserViewModel(@NonNull Application application) {
        super(application);
        mRepository = new UserRepository(application);
        mAllUsers = mRepository.getAll();
    }

    public List<User> getAll() { return mAllUsers; }

    public void insert(User user) { mRepository.insert(user); }
}
