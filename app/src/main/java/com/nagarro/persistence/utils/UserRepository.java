package com.nagarro.persistence.utils;

import android.app.Application;
import android.os.AsyncTask;

import com.nagarro.persistence.dao.UserDao;
import com.nagarro.persistence.database.AppDatabase;
import com.nagarro.persistence.entity.User;

import java.util.List;

public class UserRepository {
    private UserDao userDao;
    private List<User> allUsers;

    UserRepository(Application application) {
        AppDatabase db = AppDatabase.getAppDatabase(application);
        userDao = db.userDao();
        allUsers = userDao.getAll();
    }

    List<User> getAll(){
        return allUsers;
    }

    public void insert (User user) {
        new insertAsyncTask(userDao).execute(user);
    }

    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;

        insertAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.insertAll(params[0]);
            return null;
        }
    }
}
