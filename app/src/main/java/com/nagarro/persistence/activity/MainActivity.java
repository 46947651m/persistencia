package com.nagarro.persistence.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nagarro.persistence.R;
import com.nagarro.persistence.database.AppDatabase;

import com.nagarro.persistence.entity.User;
import com.nagarro.persistence.entity.UserAdapter;
import com.nagarro.persistence.utils.DatabaseInitializer;
import com.nagarro.persistence.utils.UserViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getName();
    private RecyclerView recycler;
    private UserAdapter adapter;
    private UserViewModel mUserViewModel;
    public static final int NEW_USER_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main);

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UserAdapter(this);
        adapter.setUsers(mUserViewModel.getAll());
        recycler.setAdapter(adapter);

        /*mUserViewModel.getAll().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> users) {
                // Update the cached copy of the words in the adapter.
                adapter.setUsers(users);
            }
        });*/


        Button clickHereBtn = (Button) findViewById (R.id.click_here_btn);
        clickHereBtn.setOnClickListener(this);

        Button buttonNew = (Button) findViewById (R.id.buttonNewUser);
        buttonNew.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        DatabaseInitializer.populateAsync(AppDatabase.getAppDatabase(this));
    }

    public void onClickNew(View v){
        Intent intent = new Intent(MainActivity.this, NewUserActivity.class);
        startActivityForResult(intent, NEW_USER_ACTIVITY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_USER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            User user = new User();
            user.setFirstName(data.getStringExtra(NewUserActivity.EXTRA_REPLY));
            mUserViewModel.insert(user);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    "Usuari no creat per falta de dades.",
                    Toast.LENGTH_LONG).show();
        }
    }
}
